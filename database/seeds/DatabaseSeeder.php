<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CreatorTableSeeder');
        $this->call('SubscriberTableSeeder');
        $this->call('TicketTableSeeder');
        $this->call('EventTableSeeder');
        $this->call('MediaTableSeeder');
    }
}
