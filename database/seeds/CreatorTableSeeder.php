<?php

use Illuminate\Database\Seeder;

class CreatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Creator::class, 15)->create();
    }
}
