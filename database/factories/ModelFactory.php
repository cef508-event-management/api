<?php
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => Hash::make('password123'),
        'is_creator' => $faker->randomElement([1, 0])
    ];
});

$factory->define(App\Subscriber::class, function (Faker\Generator $faker) {
    return [
        'gender' => $faker->randomElement(['male', 'female']),
        'age_group' => $faker->randomElement(['18+', 'all', '18-']),
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        }
    ];
});

$factory->define(App\Creator::class, function (Faker\Generator $faker) {
    return [
        'gender' => $faker->randomElement(['male', 'female']),
        'age_group' => $faker->randomElement(['18+', 'all', '18-']),
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
    ];
});

$factory->define(App\Event::class, function (Faker\Generator $faker) {
    return [
        'creator_id' => function() {
            return factory(App\Creator::class)->create()->id;
        },
        'category_id' => function() {
            return factory(App\Category::class)->create()->id;
        },
        'event_name' => $faker->catchPhrase,
        'event_date_time' => $faker->iso8601($max = 'now'),
        'location' => $faker->address,
        'capacity' => $faker->numberBetween($min = 10, $max = 2000),
        'gender_specification' => $faker->randomElement(['male', 'female']),
        'age_group' => $faker->randomElement(['18+', 'all', '18-']),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'caption' => $faker->sentence(2)
    ];
});

$factory->define(App\Ticket::class, function (Faker\Generator $faker) {
    return [
        'subscriber_id' => function() {
            return factory(App\Subscriber::class)->create()->id;
        },
        'event_id' => $faker->numberBetween($min = 1, $max = 20)
    ];
});

$factory->define(App\Media::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
        'name' => $faker->name,
        'file_path' => $faker->image($dir = '/tmp', $width = 640, $height = 480)
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->randomElement(['Wedding', 'Technology', 'Tradition']),
    ];
});
