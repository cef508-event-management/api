<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->get('users',  ['uses' => 'UserController@index']);
    $router->get('users/{id}', ['uses' => 'UserController@show']);
    $router->post('users', ['uses' => 'UserController@store']);
    $router->delete('users/{id}', ['uses' => 'UserController@delete']);
    $router->put('users/{id}', ['uses' => 'UserController@update']);

  $router->get('events',  ['uses' => 'EventController@index']);
  $router->get('events/{id}', ['uses' => 'EventController@show']);
  $router->post('events', ['uses' => 'EventController@store']);
  $router->delete('events/{id}', ['uses' => 'EventController@delete']);
  $router->put('events/{id}', ['uses' => 'EventController@update']);


  $router->get('creators',  ['uses' => 'CreatorController@index']);
  $router->get('creators/{id}', ['uses' => 'CreatorController@show']);
  $router->post('creators', ['uses' => 'CreatorController@store']);
  $router->delete('creators/{id}', ['uses' => 'CreatorController@delete']);
  $router->put('creators/{id}', ['uses' => 'CreatorController@update']);

  $router->get('categories',  ['uses' => 'CategoryController@index']);
  $router->get('categories/{id}', ['uses' => 'CategoryController@show']);
  $router->post('categories', ['uses' => 'CategoryController@store']);
  $router->delete('categories/{id}', ['uses' => 'CategoryController@delete']);
  $router->put('categories/{id}', ['uses' => 'CategoryController@update']);

  $router->get('subscribers',  ['uses' => 'SubscriberController@index']);
  $router->get('subscribers/{id}', ['uses' => 'SubscriberController@show']);
  $router->post('subscribers', ['uses' => 'SubscriberController@store']);
  $router->delete('subscribers/{id}', ['uses' => 'SubscriberController@delete']);
  $router->put('subscribers/{id}', ['uses' => 'SubscriberController@update']);

  $router->get('tickets',  ['uses' => 'TicketController@index']);
  $router->get('tickets/{id}', ['uses' => 'TicketController@show']);
  $router->post('tickets', ['uses' => 'TicketController@store']);
  $router->delete('tickets/{id}', ['uses' => 'TicketController@delete']);
  $router->put('tickets/{id}', ['uses' => 'TicketController@update']);

  $router->get('media',  ['uses' => 'MediaController@index']);
  $router->get('media/{id}', ['uses' => 'MediaController@show']);
  $router->post('media', ['uses' => 'MediaController@store']);
  $router->delete('media/{id}', ['uses' => 'MediaController@delete']);
  $router->put('media/{id}', ['uses' => 'MediaController@update']);

  $router->post('login', ['uses' => 'AuthController@login']);

  $router->get('user-events', ['uses' => 'EventController@userEvents']);
  $router->get('user-event/{event_id}', ['uses' => 'EventController@userEvent']);

  $router->get('user-tickets', ['uses' => 'TicketController@userTickets']);
  $router->get('user-ticket/{event_id}', ['uses' => 'TicketController@userTicket']);
});
