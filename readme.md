# Event Management — API

RESTful API for the Event Management project. Built with Laravel Lumen 5.6

## Official Documentation

* Clone: ``` git clone https://gitlab.com/cef508-event-management/api.git ```
* Composer: ``` composer install ```
* .env: ``` create .env file ```
* Generate app key: ``` php artisan key:generate ```
* Run: ``` php -S localhost:8000 -t public ```
* Migration and Seed: ``` php artisan migrate --seed ```

## License

The Event Management project is open-sourced software licensed under the [MIT license](https://gitlab.com/cef508-event-management/api/blob/master/LICENSE)
