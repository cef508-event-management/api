<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'media_id', 'api_token', 'is_creator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function subscriber() {
        return $this->hasOne('App\Subscriber');
    }

    public function creator() {
        return $this->hasOne('App\Creator');
    }

    public function media() {
        return $this->hasMany('App\Media');
    }
}
