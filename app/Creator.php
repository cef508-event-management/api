<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creator extends Model
{
    protected $fillable = [
        'user_id',
        'gender',
        'age_group'
    ];

    public function event() {
        return $this->hasMany('App\Event');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

}
