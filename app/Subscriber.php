<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $fillable = [
        'user_id',
        'gender',
        'age_group'
    ];

    public function ticket() {
        return $this->hasMany('App\Ticket');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
