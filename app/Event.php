<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'creator_id',
        'category_id',
        'event_name',
        'event_date_time',
        'location',
        'capacity',
        'gender_specification',
        'age_group',
        'description',
        'caption'
    ];

    public function category() {
        return $this->hasOne('App\Category');
    }

    public function creator() {
        return $this->belongsTo('App\Creator');
    }

}
