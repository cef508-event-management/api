<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'name',
        'file_path'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

}
