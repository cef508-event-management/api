<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request) {
    	$validationRules = [
    		'email' => 'required|email',
    		'password' => 'required|string'
    	];

    	$this->validate($request, $validationRules);

    	try {
    		$email = $request->input('email');
    		$password = $request->input('password');

    		$user = User::where('email', $email)->first();

    		if($user && $user->count() > 0) {
    			if(Hash::check($password, $user->password)) {
    				try {
    					$apiToken = sha1($user->id.time());
    					$user->api_token = $apiToken;
    					$user->save();
                        if($user->is_creator) {
                            $creator = $user->creator;
                            $res['creator'] = $creator;
                        } else {
                            $subscriber = $user->subscriber;
                            $res['subscriber'] = $subscriber;
                        }

    					$res['status'] = true;
    					$res['message'] = 'User successfully logged in!';
    					$res['data'] = $user;
                        $res['token'] = $user->api_token;

    					return response()->json($res, 200);
    				} catch (\Illuminate\Database\QueryException $e) {
    					$res['status'] = false;
    					$res['message'] = $e->getMessage();

    					return response()->json($res, 500);
    				}
    			} else {
    				$res['status'] = false;
    				$res['message'] = 'Incorrect authentication credentials!';

    				return response()->json($res, 401);
    			}
    		} else {
    			$res['status'] = false;
    			$res['message'] = 'User with '.$user.'/password credentials not found!';

    			return response()->json($res, 404);
    		}
    	} catch (\Illuminate\Database\QueryException $e) {
    		$res['status'] = false;
    		$res['message'] = $e->getMessage();

    		return response()->json($res, 500);
    	}
    }
}
