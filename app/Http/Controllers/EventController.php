<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Event::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::create($request->all());

        return response()->json($event, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Event::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->update($request->all());

        return response()->json($event, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function userEvents()
    {
        $user_event = Auth::user()->creator->event;

        return response()->json($user_event, 200);
    }

    public function userEvent(Request $request, $id)
    {
        $user_event = Auth::user()->creator->event->where('id', $id)->get();

        return response()->json($user_event, 200);
    }

    public function destroy($id)
    {
        Event::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
