<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use Illuminate\Database\ValidationException;
use illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Ticket::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = Auth::user();
            $ticket = Ticket::create($request->all());
            $ticket->subcriber_id = $user->id;
            $ticket->save();

            $res['message'] = 'Ticket Successfully Created';
            $res['status'] = 'true';
            $res['ticket'] = $ticket;

            return response()->json($res, 201);
        } catch(\Illuminate\Database\QueryException $e) {
            $res['message'] = $e.getMessage();
            $res['status'] = 'false';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Ticket::find($id));
    }

    public function userTickets(Request $request)
    {
        $userTicket = Auth::user()->subscriber->ticket;
        return response()->json($userTicket);
    }

    public function userTicket(Request $request, $eventId)
    {
        $ticket = Ticket::where('subcriber_id', Auth::user()->id)
                        ->where('event_id', $eventId)
                        ->first();

        return response()->json($ticket);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);
        $ticket->update($request->all());

        return response()->json($ticket, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ticket::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
