<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'subcriber_id',
        'event_id'
    ];

    public function subscriber() {
        return $this->belongsTo('App\Subscriber');
    }
}
